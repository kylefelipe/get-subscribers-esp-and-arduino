#include "LiquidCrystal.h"
#include "SoftwareSerial.h"

// código de verificação dos inscritos
// https://gist.github.com/RoboCore/14b7400bb2292f89a9efade81adff387

// Pegar a ligação do ESP desse tuto aqui
//https://circuitdigest.com/microcontroller-projects/arduino-with-esp8266-reading-data-from-internet
    
LiquidCrystal lcd(12, 11, 5, 4, 3, 2);
//String nomeCanal = "Geocast Brasil";
//unsigned int lastStringLength = nomeCanal.length();

SoftwareSerial ESP_Serial(6,7);
    
void setup() {
  lcd.begin(16, 2);
  Serial.begin(9600);
  ESP_Serial.begin(9600);
  delay(2000);
  lcd.clear();
}

int startPosition(String string){
  int remChars = 16 - string.length();
  int result;
  return remChars / 2;
};

void lcdPrint(String canal, String qtAssinantes){
  lcd.setCursor(startPosition(canal),0);
  lcd.print(canal);
  lcd.setCursor(startPosition(qtAssinantes),1);
  lcd.print(qtAssinantes);
}

void loop() {
  String nomeCanal = "Geocast Brasil";
  String qtAssinantes = "     ";
  while (ESP_Serial.available() > 0){
    qtAssinantes = ESP_Serial.readString();
    delay(10);
    Serial.print("qtAssinantes: ");
    Serial.println(qtAssinantes);
  }
  lcdPrint(nomeCanal, qtAssinantes);
  delay(5000);
  lcd.clear();
  delay(5000);
  
}
