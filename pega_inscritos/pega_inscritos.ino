#include <ArduinoJson.h>
#include <ESP8266WiFi.h>
#include <WiFiClientSecure.h>
#include "credentials.h"
//char ssid[] = "NOME_DA_REDE";       // sua rede wifi
//char password[] = "SENHA_DA_REDE";  // sua senha
//char channelId[] = "ID_DO_CANAL"; // ID do seu canal
//char apiKey[] = "CHAVE_DA_API"; // chave da API

WiFiClientSecure client;
String mensagemAnterior = "";

void setup() {
  Serial.begin(9600);
  WiFi.mode(WIFI_STA);
  WiFi.disconnect();
  delay(100);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
  }
  IPAddress ip = WiFi.localIP();
//  Serial.println(ip);
  client.setInsecure(); // necessario para algumas versoes do ESP8266 para Arduino
}

void loop() {
  checarInscritos();
  delay(10000); // checa inscritos de 10 em 10 segundos
}

void checarInscritos() {
  client.setTimeout(10000);
  if (!client.connect("www.googleapis.com", 443)) {  // 443: YouTube API SSL Port
    Serial.println("Nao conectado");
    return;
  }
  String chamarAPI = "GET https://www.googleapis.com/youtube/v3/channels?part=statistics&id=";
  chamarAPI += channelId;
  chamarAPI += "&key=";
  chamarAPI += apiKey;
  chamarAPI += " HTTP/1.1\r\n";
  client.print(chamarAPI);
  client.println(F("Connection: close"));
  if (client.println() == 0) {
    return;
  }
  char status[32] = {0};
  client.readBytesUntil('\r', status, sizeof(status));
  if (strcmp(status, "HTTP/1.1 200 OK") != 0) {
    return;
  }
  // Skip HTTP headers
  char endOfHeaders[] = "\r\n\r\n";
  if (!client.find(endOfHeaders)) {
    return;
  }
  String resposta = "";
  while (client.available()) {
    char c = client.read();
    resposta += c;
  }
  resposta = resposta.substring(4, resposta.length() - 6);
//  Serial.println("resposta");
//  Serial.println(resposta);
  // Allocate JsonBuffer
  // Use arduinojson.org/assistant to compute the capacity.
//  const size_t bufferSize = JSON_ARRAY_SIZE(1) + JSON_OBJECT_SIZE(2) + 2 * JSON_OBJECT_SIZE(4) + JSON_OBJECT_SIZE(5);
  DynamicJsonBuffer jsonBuffer(768);
  // Parse JSON object
  JsonObject& root = jsonBuffer.parseObject(resposta);
  // Test if parsing succeeds.
  if (!root.success()) {
    Serial.println("não rooteou'...");
    return;
  }
  JsonObject& items0 = root["items"][0];
  JsonObject& items0_statistics = items0["statistics"];
  //const char* items0_statistics_viewCount = items0_statistics["viewCount"]; // "2061149"
  //const char* items0_statistics_commentCount = items0_statistics["commentCount"]; // "3"
  const char* items0_statistics_subscriberCount = items0_statistics["subscriberCount"]; // "37747"
  //bool items0_statistics_hiddenSubscriberCount = items0_statistics["hiddenSubscriberCount"]; // false
  //const char* items0_statistics_videoCount = items0_statistics["videoCount"]; // "162"
  //int seguidores = atoi(items0_statistics_subscriberCount);
  String mensagem = items0_statistics_subscriberCount;
  Serial.print(mensagem);
  mensagem += "F"; //FIM
  if (mensagem != mensagemAnterior) {
//    for (int cont = 0; cont <= 10 ; cont++) {
//      if (mensagem.length() >= cont + 1) {
//        displays.Print(mensagem[cont], cont + 1);
//        delay(10);
//      } else {
//        displays.Off(cont + 1); // apaga displays que nao estiverem sendo usados
//      }
//    }
    mensagemAnterior = mensagem;
  }
  jsonBuffer.clear();
  items0_statistics_subscriberCount = "";
//   Disconnect
//  Serial.print("Qt Inscritos: ");
//  Serial.println(mensagem);
  client.stop();
//  Serial.print("Qt Inscritos: ");
//  Serial.println(mensagem);
}
