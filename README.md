# Get Subscribers - ESP and Arduino

A idéia desse projeto é:

- Usar um arduino para mostrar a quantidade de inscritos de um canal do youtube em um LCD de 16 x 2;
- Pegar essa quantidade de inscritos usando um ESP01;
- Buscar os dados acessando a API do Google usando o ESP01;

Para facilitar o envio de informações para o repositório, quando é necessário uso de credenciais, como no caso do `pega_inscritos` é utilizado um arquivo `credentials.ino` onde estarão os dados das credenciais, como por exemplo ssid da rede, senha ou o id do canal.  
Basta seguir o arquivo de exemplo que está dentro da pasta `pega_inscritos` (`credentials.ino.example`).  
Como o arquivo é `.ino` não é necessário fazer o include do mesmo no arquivo principal, mas lembre-se de manter o arquivo de credencial dentro da mesma pasta do arquivo que irá utilizar as credenciais.

## Componentes

1 - Protoboard.  
1 - Arduino UNO.  
1 - ESP01.  
1 - LCD 16 X 2.  
1 - Push button.  
1 - Potenciômetro.  
1 - Carregador de celular (fonte energia).  
1 - Cabo USB (programação do arduino e ligação do sistema no carregador)  
1 - Adaptador USB para ESP8266 ESP01 *.
Diversos Jumpers.

\* Caso não tenha o adaptador pronto para subir códigos, basta fazer uma modificação, [colocando um push button entre o GPIO0 e o GND](https://blog.eletrogate.com/gravando-programas-no-esp-01-com-o-adaptador-usb/), eu também aconselho colocar um push button entre o GND e o pino de RESET (RST) para poder fazer resets quando for subir códigos para o ESP, é muito útil quando vamos testar códigos e precisamos testar a saida serial do mesmo.

Opcional:  
Adaptador do ESP01 para protoboard.

## Fritzing

Para instalar o fritzing (linux):

```bash
sudo apt install fritzing fritzing-data fritzing-parts -y
```

O fritzing é para termos os dezenhos de ligações dos componentes.

Esse é o esquema de ligação na protoboard:

![Como ligar os componentes na protoboard](fritzing/protoboard_sketch.png)

Caso queira ter no seu FRITZING o ESP01 vc pode fazer o donwload do [arquivo dele na pasta `fritzing`](./fritzing/ESP8266%20WiFi%20Module.fzpz)

TODO:

- [ ] Módulo I2C para o LCD.  
- [ ] Trocar o arduino UNO por um Nano ou usar o ESP direto.  
- [ ] Verificar outros canais.  
- [ ] Caixa para o conjunto.  
- [ ] Botão de reset.

## USAGE

Colocar as credenciais do canal no arquivo `credencials.ino`.  
Subir o código para o Arduino.  
Subir o código para o ESP01.  
Quando ligar o sistema, ajustar o contraste do LCD usando o potenciômetro.

Fontes:

Usando LCD:

https://www.robocore.net/tutoriais/kit-iniciante-para-arduino-modulo-5

Buscando os dados dos inscritos:

Vídeo:  
https://www.robocore.net/tutoriais/kit-iniciante-para-arduino-modulo-5

Código:  
https://www.robocore.net/tutoriais/kit-iniciante-para-arduino-modulo-5
